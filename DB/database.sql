
CREATE DATABASE IF NOT EXISTS `geniat_db`

USE `geniat_db`;

DROP TABLE IF EXISTS `publicaciones`;
CREATE TABLE `publicaciones` (
  `publicaciones_id` int unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `descripcion` text,
  `creacion` datetime NOT NULL,
  `usuarios_id` int unsigned NOT NULL,
  `usuario_nombre` varchar(150) NOT NULL,
  `usuario_rol` varchar(5) NOT NULL,
  `estatus` enum('inactivo','activo') NOT NULL,
  PRIMARY KEY (`publicaciones_id`),
  KEY `FK_publicaciones_usuarios` (`usuarios_id`),
  CONSTRAINT `FK_publicaciones_usuarios` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`usuarios_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;



DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `usuarios_id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(150) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `rol` enum('1','2','3','4','5') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '1:Básico, 2:Medio, 3:Medio_Alto, 4:Alto_Medio, 5:Alto',
  PRIMARY KEY (`usuarios_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;
