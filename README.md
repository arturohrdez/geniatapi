# Api RESTFul - Geniat
Api desarrollada para procesar peticiones de tipo HTTP (GET, POST, PUT, DELETE) utilizando autenticación Json Web Token.

## Requisitos
- PHP 7.4.2 o superior
- MySQL Server 8.0.26
- Apache/2.4.38

## Introducción
Permite la consulta, inserción, actualizción y eliminado lógico de registros basado en roles de usuario, utilizando como método de aunteticación  Json Web Token.

**Roles**
Están definidos como identificadores numéricos con un rango de 1 a 5 y que se definen a continuación:
- **1** - Rol Básico (Permiso de acceso)
- **2** - Rol Medio (Permiso de acceso y consulta de registros)
- **3** - Rol Medio Alto (Permiso de acceso y agregar registros)
- **4** - Rol Alto Medio (Permiso de acceso, consulta, agregar y actualizar registros)
- **5** - Rol Alto (Permiso de acceso, consulta, agregar, actualizar y eliminar registros)

## End Points
**Registro de usuarios**
* Método: POST
* Url: https://{dominioApi}/geniatApi/v1/usuarios
Envía mediante el payload/body información para el registro de un nuevo usuario, no es necesario contar con un token, la información consta de los siguientes parametros:
```json
{
	"correo"   : "",
	"nombre"   : "",
	"apellidos": "",
	"rol"      : "",
	"password" : ""
}
```
**| Campo  | Tipo  | Obligatorio |** 
| correo | string | Si |
| nombre | string | Si |
| apellidos | string | Si |
| rol | integer | Si |
| password | string | Si |

------------


**Login**
* Método: POST
* Url: https://{dominioApi}/geniatApi/v1/usuarios/login
Identifica el usuario y genera el token mismo que deberá enviarse en los siguientes endpoints, se envía mediante el payload/body información con los siguien parametros:
```json
{
	"correo"   : "",
	"password" : ""
}
```
**| Campo  | Tipo  |  Obligatorio |** 
| correo | string | Si |
| password | string | Si |

------------


**Creación de publicación**
* Método: POST
* Url: https://{dominioApi}/geniatApi/v1/publicaciones
Envía mediante el payload/body información para el registro de una nueva publicación, requiere un token, la información consta de los siguientes parametros:
```json
{
	"titulo"     : "",
	"descripcion": ""
}
```
**| Campo  | Tipo  | Obligatorio |** 
| titulo | string | Si |
| descripcion | string | Si |
###### El token generado por el usuario se debe envíar mediantes las cabeceras utilizando el tipo "Bearer Token".

------------


**Actualización de publicación**
* Método: PUT
* Url: https://{dominioApi}/geniatApi/v1/publicaciones
Envía mediante el payload/body información para la actualización de una publicación, requiere un token, la información consta de los siguientes parametros:
```json
{
	"publicaciones_id": "",
	"titulo"          : "",
	"descripcion"     : ""
}
```
**| Campo  | Tipo  | Obligatorio |** 
| publicaciones_id | integer | Si |
| titulo | string | No |
| descripcion | string | No |
###### El token generado por el usuario se debe envíar mediantes las cabeceras utilizando el tipo "Bearer Token".

------------


**Eliminación de publicación**
* Método: DELETE
* Url: https://{dominioApi}/geniatApi/v1/publicaciones?publicaciones_id=x
Envía mediante url la varibale "publicaciones_id" con el id de la publicación que se desea eliminar, requiere un token.
**| Campo  | Tipo  | Obligatorio |** 
| publicaciones_id | integer | Si |
###### El token generado por el usuario se debe envíar mediantes las cabeceras utilizando el tipo "Bearer Token".

------------

**Consulta de publicación**
* Método: GET
* Url: https://{dominioApi}/geniatApi/v1/publicaciones
Consulta la información de todas las publicaciones registradas en el sistema. También se puede consultar una publicación en especifico mandando el parámetro "publicaciones_id" en la url del endpoint:  https://{dominioApi}/geniatApi/v1/publicaciones?publicaciones_id=x
**| Campo  | Tipo  | Obligatorio |** 
| publicaciones_id | integer | No |
###### El token generado por el usuario se debe envíar mediantes las cabeceras utilizando el tipo "Bearer Token".

## Instalación
- Clonar el repositorio dentro de la carpeta public_html de tu servidor web.
- Ejecutar script de la base de datos que se encuentra en la carpeta /DB
- Editar el archivo DB.class.php con la información de la base de datos anteriormente creada (host, dbname, user, password).
```php
    $this->pdo = new PDO('mysql:host=localhost;dbname=db;charset=utf8', 'user', 'password'); 
```











