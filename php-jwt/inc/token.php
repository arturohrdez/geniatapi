<?php
use \Firebase\JWT\JWT;

define('SERVER', "http://{$_SERVER['HTTP_HOST']}");
define('SECRET_KEY', '786c799bb66e699b17ad3c2f534d906d4ee7953f7dd612100078fad66c6299fd');  /// secret key can be a random string and keep in secret from anyone
define('ALGORITHM', 'HS256');

class TokenJwt {

    public static function getToken($token) {
        try {

            $secretKey        = base64_decode(SECRET_KEY);
            $DecodedDataArray = JWT::decode($token, $secretKey, [ALGORITHM]);
            $token            = json_decode(json_encode($DecodedDataArray));
            
            return $token;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function createToken($tipo, $usuario) {

        $tokenId = base64_encode(random_bytes(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        // a la hora de expiracion no le añado nada para poder usar el token directamente 
        // si le pongo mas tiempo tengo que esperar para poder usar el token
        $expire = $notBefore + 7200; // añado 1 hora
        $serverName = SERVER;

        // crea el array data para usuario
        // almaceno los datos del usuario para identificarlo 
        $datos = [
            'id'     => $usuario->getUsuariosId(),
            'correo' => $usuario->getCorreo(),
            'nombre' => $usuario->getNombre()." ".$usuario->getApellidos(),
            'rol'    => $usuario->getRol(),
        ];

        $data = [
            'iat'  => $issuedAt, // cuando se genero el token
            'jti'  => $tokenId, // identificador del token
            'iss'  => $serverName, // servidor
            'nbf'  => $notBefore, // se podra usar no antes de
            'exp'  => $expire, // cuando expira
            'data' => $datos
        ];
        $secretKey = base64_decode(SECRET_KEY);


        $jwt = JWT::encode(
                        $data, //Data to be encoded in the JWT
                        $secretKey, // The signing key
                        ALGORITHM
        );
        $unencodedArray = ['jwt' => $jwt];

        return $unencodedArray;
    }

}
