<?php

require_once 'model/genericDao/publicacionesDao.php';
require_once 'model/publicaciones.php';

class PublicacionesController {

    private $model;
    private $token;

    public function __CONSTRUCT($token) {
        $this->token = $token;
        $this->model = new PublicacionesDao();
    }

    //Obtiene la información de Payload/Body
    private function getBody($action = null) {
        $json  = file_get_contents('php://input');
        $_POST = json_decode($json,true);

        $publicaciones = new Publicaciones();

        if (isset($_POST["publicaciones_id"]))
            $publicaciones->setPublicacionesId($_POST["publicaciones_id"]);
        if (isset($_POST["titulo"]))
            $publicaciones->setTitulo($_POST["titulo"]);
        if (isset($_POST["descripcion"]))
            $publicaciones->setDescripcion($_POST["descripcion"]);

        if($action == null){
            $publicaciones->setCreacion(date('Y-m-d H:i:s'));
            $publicaciones->setUsuariosId($this->token->data->id);
            $publicaciones->setUsuarioNombre($this->token->data->nombre);
            $publicaciones->setUsuarioRol($this->token->data->rol);
            $publicaciones->setEstatus("activo");
        }//end if

        return $publicaciones;
    }

    public function select($params) {
        $permision = validRol($this->token->data->rol,"select");
        if($permision){
            try {
                if ($params != "") {
                    $resultDao = $this->model->selectByParams($params);
                } else {
                    $resultDao = $this->model->selectAll();
                }//end if

                return $resultDao;

            } catch (Exception $e) {
                return $e->getMessage();
            }
        }else{
            $response_error = [
                "status" => "error",
                "message" => "Permisos denegados",
            ];

            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode($response_error);
            die();
        }//end if
    }//end function

    public function insert() {
        $permision = validRol($this->token->data->rol,"insert");
        if($permision){
            try {
                $publicaciones = $this->getBody();

                //Valida el modelo 
                $valid_model = $this->validModel($publicaciones,"insert");
                $res_valid   = $valid_model["status"];
                if($res_valid == "error"){
                    $response_error = [
                        "status" => $res_valid,
                        "message" => $valid_model["message"],
                    ];

                    header('Cache-Control: no-cache, must-revalidate');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Content-type: application/json');
                    echo json_encode($response_error);
                    return false;
                }//end if

                // inserto nuevo registro
                $last_insert = $this->model->insert($publicaciones);
                return $last_insert;
            } catch (Exception $e) {
                echo $e->getMessage();
                return null;
            }
        }else{
            $response_error = [
                "status" => "error",
                "message" => "Permisos denegados",
            ];

            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode($response_error);
            die();
        }//end if
    }//end function

    public function edit(){
        $permision = validRol($this->token->data->rol,"update");
        if($permision){
            try {
                $publicaciones = $this->getBody("update");

                //Valida el modelo 
                $valid_model = $this->validModel($publicaciones,"update");
                $res_valid   = $valid_model["status"];
                if($res_valid == "error"){
                    $response_error = [
                        "status" => $res_valid,
                        "message" => $valid_model["message"],
                    ];

                    header('Cache-Control: no-cache, must-revalidate');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Content-type: application/json');
                    echo json_encode($response_error);
                    return false;
                }//end if

                $resultado = $this->model->update($publicaciones);
                return $resultado;

            } catch (Exception $e) {
                echo $e->getMessage();
                return null;
            }
        }else{
            $response_error = [
                "status" => "error",
                "message" => "Permisos denegados",
            ];

            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode($response_error);
            die();
        }
    }//end function

    public function delete($params) {
        $permision = validRol($this->token->data->rol,"delete");
        if($permision){
            try {
                $model_publicaciones = $this->model->selectById($params["valor"]);
                if(is_null($model_publicaciones)){
                        $response_error = ["status"=>"error","message"=>"Publicación no encontrada"];

                        header('Cache-Control: no-cache, must-revalidate');
                        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                        header('Content-type: application/json');
                        echo json_encode($response_error);
                        return false;
                }else{
                    //Borrado Lógico
                    $result_delete = $this->model->delete($model_publicaciones->getPublicacionesId());
                    return $result_delete;
                }//end if

            } catch (Exception $e) {
                return null;
            }
        }else{
            $response_error = [
                "status" => "error",
                "message" => "Permisos denegados",
            ];

            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode($response_error);
            die();
        }
    }//end if


    private function validModel($data,$action){
        $titulo      = $data->getTitulo();
        $descripcion = $data->getDescripcion();

        if($action == "update"){
            $publicaciones_id      = $data->getPublicacionesId();
            if(empty($publicaciones_id) || !isset($publicaciones_id)){
                return ["status"=>"error","message"=>"Id es requerido"];
            }else {
                //Busca publicación por Id
                $model_publicaciones = $this->model->selectById($publicaciones_id);
                if(is_null($model_publicaciones)){
                    return ["status"=>"error","message"=>"Publicación no encontrada"];
                }//end if
            }//end if
        }else{
            if(empty($titulo) || !isset($titulo)){
                return ["status"=>"error","message"=>"Titulo es requerido"];
            }//end if

            if(empty($descripcion) || !isset($descripcion)){
                return ["status"=>"error","message"=>"Descripcion es rquerida"];
            }//end if
        }//end if


        return ["status"=>"success"];
    }//end if

}
