<?php

require_once 'model/genericDao/usuariosDao.php';
require_once 'model/usuarios.php';
require_once 'core.php';

class UsuariosController {

    private $model;
    private $token;

    public function __CONSTRUCT($token) {
        $this->token = $token;
        $this->model = new UsuariosDao();
    }

    //Obtiene la información de Payload/Body
    private function getBody() {
        $json  = file_get_contents('php://input');
        $_POST = json_decode($json,true);

        $usuarios = new Usuarios();

        if (isset($_POST["correo"]))
            $usuarios->setCorreo($_POST["correo"]);
        if (isset($_POST["nombre"]))
            $usuarios->setNombre($_POST["nombre"]);
        if (isset($_POST["apellidos"]))
            $usuarios->setApellidos($_POST["apellidos"]);
        if (isset($_POST["rol"]))
            $usuarios->setRol($_POST["rol"]);
        if (isset($_POST['password']))
            $usuarios->setPassword($_POST["password"]);

        return $usuarios;
    }

    public function login() {
        $usuario       = $this->getBody();
        $model_usuario = $this->model->selectByCorreo($usuario->getCorreo());

        // si existe el usuario con dicho correo
        if ($model_usuario != null) {
            // verifica que el password sea el corrrecto
            if (password_verify($usuario->getPassword(), $model_usuario->getPassword())) {
                // comprueba si necesita crear el hash
                if (password_needs_rehash($model_usuario->getPassword(), PASSWORD_DEFAULT)) {
                    // genera el hash y guardo la nueva contraseña
                    $model_usuario->password = password_hash($model_usuario->getPassword(), self::HASH);
                    $this->model->update($model_usuario);
                }//end if


                // creo el token usando JWT
                $unencodedArray = TokenJwt::createToken("usuarios", $model_usuario);

                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode([
                    'status' => "success",
                    'model' => $model_usuario,
                    'token' => $unencodedArray
                ]);
                
            } else {
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode([
                    'status' => "error",
                    'message'    => "correo o clave incorrectos",
                ]);

                //return null;
            }
        }else{

            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode([
                    'status' => "error",
                    'message'   => "información no encontrada",
                ]);
        }
    }

    public function select($params) {
        $permision = validRol($this->token->data->rol,"select");

        if($permision){
            try {
                if ($params != "") {
                    $resultDao = $this->model->selectByParams($params);
                } else {
                    $resultDao = $this->model->selectAll();
                }//end if

                return $resultDao;

            } catch (Exception $e) {
                return $e->getMessage();
            }
        }else{

            $response_error = [
                "status" => "error",
                "message" => "Permisos denegados",
            ];

            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode($response_error);
            die();
        }//end if
    }//end function

    public function insert() {
        try {
            $usuario = $this->getBody();
            
            if(!empty($usuario->getPassword())){
                //Encripta password
                $password_encrypt = password_hash($usuario->getPassword(), PASSWORD_DEFAULT);
                $usuario->setPassword($password_encrypt);   
            }

            //Valida el modelo 
            $valid_model = $this->validModel($usuario,"insert");
            $res_valid   = $valid_model["status"];
            if($res_valid == "error"){
                $response_error = [
                    "status" => $res_valid,
                    "message" => $valid_model["message"],
                ];

                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');
                echo json_encode($response_error);
                return false;
            }//end if


            // inserto nuevo registro
            $last_insert = $this->model->insert($usuario);
            return $last_insert;
        } catch (Exception $e) {
            echo $e->getMessage();
            return null;
        }
    }//end function

    private function validModel($data,$action){
        $nombre    = $data->getNombre();
        $apellidos = $data->getApellidos();
        $correo    = $data->getCorreo();
        $password  = $data->getPassword();
        $rol       = $data->getRol();
        $roles     = [1,2,3,4,5];

        if(empty($nombre) || !isset($nombre)){
            return ["status"=>"error","message"=>"Nombre es requerido"];
        }//end if

        if(empty($apellidos) || !isset($apellidos)){
            return ["status"=>"error","message"=>"Apellidos son requeridos"];
        }//end if

        if(empty($correo) || !isset($correo)){
            return ["status"=>"error","message"=>"Correo es requerido"];
        }else{
            $model_usuario = $this->model->selectByCorreo($correo);
            if(!is_null($model_usuario)){
                return ["status"=>"error","message"=>"El correo ya existe"];  
            }//end if
        }//end if

        if(empty($password) || !isset($password)){
            return ["status"=>"error","message"=>"Password es requerido"];
        }//end if

        if(empty($rol) || !isset($rol)){
            return ["status"=>"error","message"=>"Rol es requerido"];
        }else{
            if (!in_array($rol, $roles)) {
                return ["status"=>"error","message"=>"Número de rol erroneo"];
            }//end if
        }//end if

        return ["status"=>"success"];
    }//end if
}
