<?php 
function getMethod(){
	return $_SERVER['REQUEST_METHOD'];
}//end function

function getController($req_uri = null){
	if(!is_null($req_uri)){
		$req_uri = explode("/", $req_uri);
		if (isset($req_uri[3]) && $req_uri[3] != "") {
		    $controlador = $req_uri[3];
		    // si hay parametros enviados por get
		    // usuario?rol=1
		    if (count($_GET) > 0) {
		        $controladorConVariable = explode("?", $controlador);
		        if (isset($controladorConVariable[1])) {
		            $controlador = $controladorConVariable[0];
		        }//end if
		    }//end if
		}//end if
	}else{
		return false;
	}//end if

	return $controlador;
}//end function

function getAction($req_uri = null){
	if(!is_null($req_uri)){
		$req_uri = explode("/", $req_uri);
		$id      = "";
		$accion  = "";
		if (isset($req_uri[4]) && $req_uri[4] != "") {
		    $inicia_sesion = strtolower($req_uri[4]); //Cambia string de la acción a minúsculas 
		    if (strcasecmp($inicia_sesion,"login") === 0) {//Válida a nivel binario los strings sin importar mayúsculas y minúsculas
		        $accion = "login";
		    } else {
		        $id = $req_uri[4];

		        if (count($_GET) > 0) {
		            $idConVariable = explode("?", $id);
		            if (isset($idConVariable[1])) {
		                $id = $idConVariable[0];
		            }
		        }
		    }
		}//end if
	}else{
		return false;
	}//end if
	return $accion;
}//end function

function getParameter($req_uri = null){
	if(!is_null($req_uri)){
		$req_uri = explode("/", $req_uri);
		$id      = "";
		$accion  = "";
		if (!isset($req_uri[4])) {
			$id = end($req_uri);

			if (count($_GET) > 0) {
				$variable = explode("?", $id);
				$variable = explode("=",end($variable));
				$campo    = current($variable);
				$valor    = end($variable);

				return ["campo"=>$campo,"valor"=>$valor];

	        }//end fi
		}else{
			return false;
		}//end if
	}else{
		return false;
	}//end if
}//end function

function validRol($rol,$action){
    switch ($rol) {
        case 1:
            $permisions = $action != "login" ? false : true;
            break;
        case 2:
            $permisions = ($action != "login" && $action != "select") ? false : true;
            break;
        case 3:
            $permisions = ($action != "login" && $action != "insert") ? false : true;
            break;
        case 4:
            $permisions = ($action != "login" && $action != "select" && $action != "insert" && $action != "update") ? false : true;
            break;
        case 5:
            $permisions = true;
            break;
    }//end switch 

    return $permisions;
}//end function



?>