<?php

class DB {

    private static $instance_db;
    private $pdo;

    /**
     * Devuelve la instancia de esta clase
     * si no existe la crea y la devuelve
     *  
     * @return Database instancia de esta clase
     */
    public static function getInstance() {
        if (!self::$instance_db) {
            self::$instance_db = new self();
        }
        return self::$instance_db;
    }

    /**
     * Crea la conexion a la base de datos
     */
    public function __construct() {
        $this->pdo = new PDO('mysql:host=database;dbname=geniat_db;charset=utf8', 'root', 'tiger');
        
        // Desactivar preparaciones emuladas. Esto asegura que obtenga declaraciones preparadas.
        // y evitamos inyeccion sql
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Devuelve la conexion a la bd
     *  
     * @return PDO conexion a la bd
     */
    public function getConnection() {
        return $this->pdo;
    }
    
}

?>