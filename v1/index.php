<?php
// Habilita los metodos get, post, put y delete
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");


//Database Class 
require_once "database/DB.class.php";
//JWT;
/**
 * @ref https://github.com/firebase/php-jwt
 */
require_once "../php-jwt/src/BeforeValidException.php";
require_once "../php-jwt/src/ExpiredException.php";
require_once "../php-jwt/src/SignatureInvalidException.php";
require_once "../php-jwt/src/JWT.php";
require_once "../php-jwt/inc/token.php";

//Core Functions
require_once "core.php";

//Obtiene el método de la solicitud
//GET, POST, OPTIONS, PUT, DELETE
$method  = getMethod();

//Decodifica el controlador
$controller = getController($_SERVER['REQUEST_URI']);

//Decodifica acción
$action      = getAction($_SERVER['REQUEST_URI']);

//Decodifica Parametros
$params      = getParameter($_SERVER['REQUEST_URI']);

$headers = apache_request_headers();
// si no existe controlador o accion muestro error
if (!isset($controller) || !isset($action)) {
    decodeError(404, "No existe el objeto ".$controller);
} else {

    // Carga del controlador
    $pathController = "controller/$controller.controller.php";
    if(!file_exists($pathController)){
        decodeError(404, "No existe el objeto ".$controller);
        return;
    }else{
        require_once $pathController;
        $nameController = ucwords($controller) . 'Controller';
    }//end if

    // Login Usuario
    if ($action == "login" && $method == "POST" && $controller == "usuarios") {
        $controller = new $nameController(null);
        $res        = $controller->$action();
    } elseif($action == "" && $method == "POST" && $controller == "usuarios"){ //Registro de usuarios
        $controller = new $nameController(null);
        $res        = $controller->insert();
        echo $res;
    }else {
        // Acciones generadas según el método enviado
        // comprueba el token JWT
        $jwt = @sscanf($headers["Authorization"], 'Bearer %s');
        $token = TokenJwt::getToken($jwt[0]);

        if ($token != null) {
            //Llammado al controlador
            $controller = new $nameController($token);

            // llamo a la funcion segun el método
            // GET: Consulta, POST: Inserta, PUT: Actualiza, DELETE: Elimina
            switch ($method) {
                case 'GET':
                    $res = $controller->select($params);
                    break;
                case 'POST':
                    $res = $controller->insert();
                    break;
                case 'PUT':
                    $res = $controller->edit();
                    break;
                case 'DELETE':
                    $res = $controller->delete($params);
                    break;
            }//end switch
            
            switch ($method) {
                case "GET":
                    decodeJson($res);
                    break;
                case "POST":
                case "PUT":
                case "DELETE":
                    echo $res;
                    break;
            }
        } else {
            decodeError("401", "El token no es valido, vuelve a iniciar sesión.");
        }
    }
}

function decodeJson($res) {
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    echo json_encode($res);
}

function decodeError($error, $mensaje) {

    $response_error = [
        "error" => $error,
        "mensaje" => $mensaje,
    ];

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    echo json_encode($response_error);
}

?>