<?php

class PublicacionesDao {

    private $pdo;

    public function __CONSTRUCT() {
        try {
            $db = DB::getInstance();
            $this->pdo = $db->getConnection();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function selectById($id) {
        $stm = $this->pdo->prepare("SELECT * FROM publicaciones WHERE publicaciones_id = ? and estatus = 'activo'");
        $stm->execute([$id]);
        $p   = $stm->fetch(PDO::FETCH_OBJ);

        if ($p != null) {
            $publicacion = new Publicaciones();
            $publicacion->crear($p->publicaciones_id, $p->titulo, $p->descripcion, $p->creacion, $p->usuarios_id, $p->usuario_nombre, $p->usuario_rol, $p->estatus);
            $res = $publicacion;
        } else {
            $res = null;
        }
        return $res;
    }

    public function selectByParams($params) {
        $stm         = $this->pdo->prepare("SELECT * FROM publicaciones WHERE ".$params["campo"]." = ? and estatus = 'activo'");
        $stm->execute([$params["valor"]]);
        $publicacionesPdo = $stm->fetchAll(PDO::FETCH_OBJ);
        $publicaciones    = [];
        if ($publicacionesPdo != null) {
            foreach ($publicacionesPdo as $p) {
                $publicacion = new Publicaciones();
                $publicacion->crear($p->publicaciones_id, $p->titulo, $p->descripcion, $p->creacion, $p->usuarios_id, $p->usuario_nombre, $p->usuario_rol, $p->estatus);
                //$res = $usuario;
                array_push($publicaciones, $publicacion);
            }
            return $publicaciones;
        } else {
            $res = null;
        }
    }

    public function selectAll() {
        $stm              = $this->pdo->prepare("SELECT * FROM publicaciones where estatus = 'activo'");
        $stm->execute();
        $publicacionesPdo = $stm->fetchAll(PDO::FETCH_OBJ);
        $publicaciones    = [];
        foreach ($publicacionesPdo as $p) {
            $publicacion = new Publicaciones();
            $publicacion->crear($p->publicaciones_id, $p->titulo, $p->descripcion, $p->creacion, $p->usuarios_id, $p->usuario_nombre, $p->usuario_rol, $p->estatus);
            array_push($publicaciones, $publicacion);
        }
        return $publicaciones;
    }


    public function insert($data) {

        $sql = "INSERT INTO publicaciones (titulo,descripcion,creacion,usuarios_id,usuario_nombre,usuario_rol,estatus) VALUES (?, ?, ?, ?, ?, ?, ?)";
        
        $this->pdo->prepare($sql)->execute(
                [$data->getTitulo(), $data->getDescripcion(), $data->getCreacion(), $data->getUsuariosId(), $data->getUsuarioNombre(), $data->getUsuarioRol(),$data->getEstatus()]
        );
        
        return $data->getTitulo();
    }

    public function update($data) {
        $sql = "UPDATE 
                    publicaciones SET 
                        titulo = IFNULL(:titulo, titulo),
                        descripcion = IFNULL(:descripcion, descripcion)
                WHERE 
                    publicaciones_id=:publicaciones_id";

        $update = $this->pdo->prepare($sql);
        $update->bindValue(':titulo', $data->getTitulo(), PDO::PARAM_STR);
        $update->bindValue(':descripcion', $data->getDescripcion(), PDO::PARAM_STR);
        $update->bindValue(':publicaciones_id', $data->getPublicacionesId(), PDO::PARAM_INT);
        $update->execute();

        return $data->getPublicacionesId();
    }

    public function delete($id) {
         $sql = "UPDATE 
                    publicaciones SET 
                        estatus =:estatus
                WHERE 
                    publicaciones_id=:id";

        $update = $this->pdo->prepare($sql);
        $update->bindValue(':estatus', 'inactivo', PDO::PARAM_STR);
        $update->bindValue(':id', $id, PDO::PARAM_INT);
        $update->execute();

        return $id;
    }

}
