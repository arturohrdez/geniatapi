<?php

class UsuariosDao {

    private $pdo;

    public function __CONSTRUCT() {
        try {
            $db = DB::getInstance();
            $this->pdo = $db->getConnection();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function selectByCorreo($correo) {
        $stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE correo = ?");
        $stm->execute([$correo]);
        $p   = $stm->fetch(PDO::FETCH_OBJ);

        if ($p != null) {
            $usuario = new Usuarios();
            $usuario->crear($p->usuarios_id, $p->nombre, $p->apellidos, $p->correo, $p->password, $p->rol);
            $res = $usuario;
        } else {
            $res = null;
        }
        return $res;
    }

    public function selectByParams($params) {
        $stm         = $this->pdo->prepare("SELECT * FROM usuarios WHERE ".$params["campo"]." = ? AND usuarios_id > 1");
        $stm->execute([$params["valor"]]);
        $usuariosPdo = $stm->fetchAll(PDO::FETCH_OBJ);
        $usuarios    = [];
        if ($usuariosPdo != null) {
            foreach ($usuariosPdo as $p) {
                $usuario = new Usuarios();
                $usuario->crear($p->usuarios_id, $p->nombre, $p->apellidos, $p->correo, $p->password, $p->rol);
                //$res = $usuario;
                array_push($usuarios, $usuario);
            }
            return $usuarios;
        } else {
            $res = null;
        }
    }

    public function selectAll() {
        $stm         = $this->pdo->prepare("SELECT * FROM usuarios where usuarios_id > 1");
        $stm->execute();
        $usuariosPdo = $stm->fetchAll(PDO::FETCH_OBJ);
        $usuarios    = [];
        foreach ($usuariosPdo as $p) {
            $usuario = new Usuarios();
            $usuario->crear($p->usuarios_id, $p->nombre, $p->apellidos, $p->correo, $p->password, $p->rol);
            array_push($usuarios, $usuario);
        }
        return $usuarios;
    }

    public function insert($data) {

        $sql = "INSERT INTO usuarios (nombre,apellidos,correo,password,rol) VALUES (?, ?, ?, ?, ?)";
        
        $this->pdo->prepare($sql)->execute(
                array($data->getNombre(), $data->getApellidos(), $data->getCorreo(), $data->getPassword(), $data->getRol())
        );

        return $data->getCorreo();
    }

}
