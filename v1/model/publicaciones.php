<?php

class Publicaciones implements JsonSerializable {

    private $publicaciones_id;
    private $titulo;
    private $descripcion;
    private $creacion;
    private $usuarios_id;
    private $usuario_nombre;
    private $usuario_rol;
    private $estatus;

    function __construct() {
        
    }

    function crear($publicaciones_id, $titulo, $descripcion, $creacion, $usuarios_id, $usuario_nombre, $usuario_rol, $estatus) {
        $this->publicaciones_id = $publicaciones_id;
        $this->titulo           = $titulo;
        $this->descripcion      = $descripcion;
        $this->creacion         = $creacion;
        $this->usuarios_id      = $usuarios_id;
        $this->usuario_nombre   = $usuario_nombre;
        $this->usuario_rol      = $usuario_rol;
        $this->estatus          = $estatus;
    }
    
    function getPublicacionesId() {
        return $this->publicaciones_id;
    }

    function setPublicacionesId($PublicacionesId) {
        return $this->publicaciones_id = $PublicacionesId;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function setTitulo($Titulo) {
        return $this->titulo = $Titulo;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($Descripcion) {
        $this->descripcion = $Descripcion;
    }

    function getCreacion() {
        return $this->creacion;
    }

    function setCreacion($Creacion) {
        $this->creacion = $Creacion;
    }

    function getUsuariosId() {
        return $this->usuarios_id;
    }

    function setUsuariosId($UsuariosId) {
        $this->usuarios_id = $UsuariosId;
    }

    function getUsuarioNombre() {
        return $this->usuarios_nombre;
    }

    function setUsuarioNombre($UsuarioNombre) {
        $this->usuarios_nombre = $UsuarioNombre;
    }

    function getUsuarioRol() {
        return $this->usuario_rol;
    }

    function setUsuarioRol($UsuarioRol){
        $this->usuario_rol = $UsuarioRol;
    }

    function getEstatus() {
        return $this->estatus;
    }

    function setEstatus($Estatus) {
        $this->estatus = $Estatus;
    }
    
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }

}
