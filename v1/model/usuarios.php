<?php

class Usuarios implements JsonSerializable {

    private $usuarios_id;
    private $nombre;
    private $apellidos;
    private $correo;
    private $password;
    private $rol;

    function __construct() {
        
    }

    function crear($usuarios_id, $nombre, $apellidos, $correo, $password, $rol) {
        $this->usuarios_id = $usuarios_id;
        $this->nombre      = $nombre;
        $this->apellidos   = $apellidos;
        $this->correo      = $correo;
        $this->password    = $password;
        $this->rol         = $rol;
    }
    
    function getUsuariosId() {
        return $this->usuarios_id;
    }

    function setUsuariosId($UsuariosId) {
        return $this->usuarios_id = $UsuariosId;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($Nombre) {
        return $this->nombre = $Nombre;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function setApellidos($Apellidos) {
        $this->apellidos = $Apellidos;
    }

    function getCorreo() {
        return $this->correo;
    }

    function setCorreo($Correo) {
        $this->correo = $Correo;
    }

    function getPassword() {
        return $this->password;
    }

    function setPassword($Password) {
        $this->password = $Password;
    }

    function getRol() {
        return $this->rol;
    }

    function setRol($Rol) {
        $this->rol = $Rol;
    }
    
    public function jsonSerialize() {
        $vars = get_object_vars($this);
        return $vars;
    }

}
